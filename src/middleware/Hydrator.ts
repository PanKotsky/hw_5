class Hydrator {

    hydrateRecord(schema: object, data: object, newId: number|null = null): object {
        let record = {};
        let key: string, value;

        for ([key, value] of Object.entries(schema)) {
            if (key === 'id') {
                // @ts-ignore
                record[key] = newId || data[key];
            }  else if (value === Date) {
                // @ts-ignore
                record[key] = new Date().toISOString();
            } else {
                // @ts-ignore
                record[key] = value(data[key]);
            }
        }

        return record;
    }
}

export = Hydrator;
