"use strict";
class Hydrator {
    hydrateRecord(schema, data, newId = null) {
        let record = {};
        let key, value;
        for ([key, value] of Object.entries(schema)) {
            if (key === 'id') {
                // @ts-ignore
                record[key] = newId || data[key];
            }
            else if (value === Date) {
                // @ts-ignore
                record[key] = new Date().toISOString();
            }
            else {
                // @ts-ignore
                record[key] = value(data[key]);
            }
        }
        return record;
    }
}
module.exports = Hydrator;
