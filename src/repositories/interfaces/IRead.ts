import Types = require('../../types/types');
export interface IRead<T> {
    findAll(params: Types.ParamsForPagination): Promise<Array<object>>;
    findById(id: string|number): Promise<object|null|undefined>;
}
