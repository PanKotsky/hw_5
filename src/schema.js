"use strict";
const NewpostSchema = {
    schema: {
        id: Number,
        title: String,
        text: String,
        createDate: Date,
    },
};
module.exports = {
    NewpostSchema: NewpostSchema
};
