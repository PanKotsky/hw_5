export type ParamsForPagination = {
  page: number,
  size: number
};
