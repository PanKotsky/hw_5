export class Newpost {
    public readonly pk: string = 'id';
    private readonly table: string = 'newpost';
    // @ts-ignore
    private id: number;
    // @ts-ignore
    private title: string;
    // @ts-ignore
    private text: string;
    // @ts-ignore
    private createDate: Date;

    public getId(): number {
        return this.id;
    }

    public setId(id: number): Newpost  {
        this.id = id;

        return this;
    }

    public getTitle(): string {
        return this.title;
    }

    public setTitle(title: string): Newpost {
        this.title = title;

        return this;
    }

    public getText(): string {
        return this.text;
    }

    public setText(text: string): Newpost {
        this.text = text;

        return this;
    }

    public getDate(): Date {
        return this.createDate;
    }

    public setDate(createDate: Date): Newpost {
        this.createDate = createDate;

        return this;
    }
}
