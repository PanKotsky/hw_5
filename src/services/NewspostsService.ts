import NewpostRepository = require('../repositories/Newpost');
import Types = require('../types/types');

class NewspostsService {
    getAll(params: Types.ParamsForPagination): Promise<Array<object>> {
        return NewpostRepository.findAll(params);
    }

    getById(id: number|string): Promise<object|null|undefined> {
        return NewpostRepository.findById(id);
    }

    create(data: object): Promise<string|number|void> {
        return NewpostRepository.create(data);
    }

    update(id: number|string, update: object): Promise<object|null|void> {
        return NewpostRepository.updateById(id, update);
    }

    delete(id: number|string): Promise<string|number|null|void> {
        return NewpostRepository.deleteById(id);
    }
}

export = new NewspostsService;
