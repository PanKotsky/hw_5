"use strict";
const NewpostRepository = require("../repositories/Newpost");
class NewspostsService {
    getAll(params) {
        return NewpostRepository.findAll(params);
    }
    getById(id) {
        return NewpostRepository.findById(id);
    }
    create(data) {
        return NewpostRepository.create(data);
    }
    update(id, update) {
        return NewpostRepository.updateById(id, update);
    }
    delete(id) {
        return NewpostRepository.deleteById(id);
    }
}
module.exports = new NewspostsService;
