const NewpostsService = require('./src/services/NewspostsService');
const express = require('express')
const app = express();
const cors = require('cors');
const port = process.env.PORT || 3000;
const host = process.env.HOST;

app.use(cors());

app.get("/newsposts", function (req, res) {
    let params = {
        page: req.params["page"] || 0,
        size: req.params["size"] || 10
    };
    NewpostsService.getAll(params)
        .then(result => res.status(200).send(result))
        .catch(err => res.status(500).send(err));
})

app.get("/newsposts/:id", function (req, res) {
    const id = req.params["id"];
    NewpostsService.getById(id)
        .then(function (result) {
            (!result) ? res.status(404).send(result) : res.status(200).send(result);
        })
        .catch(err => res.status(500).send(err));
})

app.post("/newsposts", express.json(),function (req, res) {
    const data = req.body;
    NewpostsService.create(data)
        .then(result => res.status(200).send({id: result}))
        .catch(err => res.sendStatus(500).send(err));
})

app.put("/newsposts/:id", express.json(),function (req, res) {
    const id = req.params["id"];
    const data = req.body;
    NewpostsService.update(id, data)
        .then(function (result) {
            (!result) ? res.status(404).send(result) : res.status(200).send({id: result});
        })
        .catch(err => res.status(500).send(err));
})

app.delete("/newsposts/:id", express.json(),function (req, res) {
    const id = req.params["id"];
    NewpostsService.delete(id)
        .then(function (result) {
            (!result) ? res.status(404).send(result) : res.status(200).send(result);
        })
        .catch(err => res.status(500).send(err));
})

app.listen(port, host);