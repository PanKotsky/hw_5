import axios from "axios";

export const mixin = {
  data() {
    return {
      model: null,
    };
  },
  methods: {
    getServerUrl() {
      const host = process.env.VUE_APP_HOST;
      const port = process.env.VUE_APP_PORT;
      return "http://" + host + ":" + port;
    },
    async getAllNews() {
      const uri = this.getServerUrl() + "/newsposts";
      return await axios.get(uri).then((res) => {
        this.model = res.data;
      });
    },
    async getNews(id) {
      const uri = this.getServerUrl() + "/newsposts/" + id;
      return await axios
        .get(uri)
        .then((res) => {
          console.log(res);
          this.model = res.data;
        })
        .catch((err) => {
          console.error(err);
        });
    },
    async addNews(data) {
      const uri = this.getServerUrl() + "/newsposts";
      console.log(data);
      await axios.post(uri, data).then((res) => {
        console.log(res);
      });
    },
    async updateNews(data) {
      const uri = this.getServerUrl() + "/newsposts/" + data.id;
      await axios.put(uri, data).then((res) => {
        console.log(res);
      });
    },
    async deleteNews(id) {
      const uri = this.getServerUrl() + "/newsposts/" + id;
      await axios.delete(uri, { id: id }).then((res) => {
        console.log(res);
      });
    },
  },
};
